# Xo

Start all services:
```bash
docker-compose up
```

Apply migrations:
```bash
docker-compose run --rm web python run_db.py
```

## Using example

Create game:
POST localhost:8080/game
{
    "field_size": 3,
}

Make move:
PATCH localhost:8080/game/42
{
    "position": 3
}

Data contract description: `app/schemas.py`

Position indexes for 4x4 field:
```
| 0| 1| 2| 3|
| 4| 5| 6| 7|
| 8| 9|10|11|
|12|13|14|15|
```
## Notes

All game logic is placed serverside.

CPU intensive operations such as calculation of next move of machine are perforemed in Process executor pool.

Game session is stored in redis.

After end of game, game details are written to postgres database.
