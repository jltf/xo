import pytest

from game import XOGame, MoveExists, generate_move


def build_moves_list(moves_map):
    xs = []
    os = []
    for num, char in enumerate(moves_map):
        if char == "X":
            xs.append(num)
        elif char == "O":
            os.append(num)
    moves = []
    for i in range(len(moves_map)):
        try:
            moves.append(xs[i])
        except IndexError:
            pass
        try:
            moves.append(os[i])
        except IndexError:
            pass

    return moves


def test_moves_map():
    game = XOGame(3, [0, 4, 8], 3)
    expected_moves_map = [
        ['X', '.', '.'],
        ['.', 'O', '.'],
        ['.', '.', 'X']
    ]
    assert expected_moves_map == game._moves_map


def test_same_move():
    game = XOGame(3, [0, 4, 8], 3)
    with pytest.raises(MoveExists):
        game.make_move(8)


def test_moves_making_a_move():
    game = XOGame(3, [0, 4, 8], 3)
    game.make_move(6)
    expected_moves_map = [
        ['X', '.', '.'],
        ['.', 'O', '.'],
        ['O', '.', 'X']
    ]
    assert expected_moves_map == game._moves_map


def test_fetch_new_moves():
    game = XOGame(3, [0, 4, 8], 3)
    game.make_move(6)
    game.make_move(2)
    assert [6, 2] == game.fetch_new_moves()


def test_resseting_new_moves():
    game = XOGame(3, [0, 4, 8], 3)
    game.make_move(6)
    game.make_move(2)
    game.fetch_new_moves()
    game.make_move(1)
    game.make_move(3)
    assert len(game.fetch_new_moves()) == 2


def test_all_moves_are_made():
    game = XOGame(3, [0, 4, 8, 1, 2, 3, 5, 6, 7], 3)
    assert game.all_moves_are_made() is True


def test_generated_move_should_be_in_empty_cell():
    field_size = 3
    moves = {0, 4, 8, 1, 2, 3, 5, 6}
    assert generate_move(field_size, moves) == 7


def test_winning_move_of_x():
    moves_map = (
        ".O...O."
        "...O..."
        "X......"
        ".OOXXX."
        "..X..O."
        ".X.O..."
        "XO..X.."
    )
    moves = build_moves_list(moves_map)
    game = XOGame(7, moves, 5)
    result = game.make_move(18)[0]
    assert result is True


def test_not_winning_move_of_x():
    moves_map = (
        ".O...O."
        "...O..."
        "X......"
        ".OOXXX."
        "..X..O."
        ".X.O..."
        "XO..X.."
    )
    moves = build_moves_list(moves_map)
    game = XOGame(7, moves, 5)
    result = game.make_move(17)[0]
    assert result is False


def test_not_winning_move_of_o():
    moves_map = (
        "OOO.OOO"
        "XXX.XXX"
        "...X..."
        "......."
        "......."
        "......."
        "......."
    )
    moves = build_moves_list(moves_map)
    game = XOGame(7, moves, 5)
    result = game.make_move(48)[0]
    assert result is False


def test_winning_move_of_o():
    moves_map = (
        "OOO.OOO"
        "XXX.XXX"
        "...X..."
        "......."
        "......."
        "......."
        "......."
    )
    moves = build_moves_list(moves_map)
    game = XOGame(7, moves, 5)
    result = game.make_move(3)[0]
    assert result is True


def test_winning_move_of_x_horizontal():
    moves_map = (
        ".OO.OO."
        ".XX.XX."
        "......."
        "......."
        "......."
        "......."
        "......."
    )
    moves = build_moves_list(moves_map)
    game = XOGame(7, moves, 5)
    result = game.make_move(10)[0]
    assert result is True


def test_winning_move_of_x_vertical():
    moves_map = (
        ".OO.OO."
        "...X..."
        "...X..."
        "......."
        "...X..."
        "...X..."
        "......."
    )
    moves = build_moves_list(moves_map)
    game = XOGame(7, moves, 5)
    result = game.make_move(24)[0]
    assert result is True


def test_winning_move_of_x_diagonal_up():
    moves_map = (
        ".OO.OO."
        ".....X."
        "....X.."
        "......."
        "..X...."
        ".X....."
        "......."
    )
    moves = build_moves_list(moves_map)
    game = XOGame(7, moves, 5)
    result = game.make_move(24)[0]
    assert result is True


def test_winning_move_of_x_diagonal_down():
    moves_map = (
        ".OO.OO."
        ".X....."
        "..X...."
        "......."
        "....X.."
        ".....X."
        "......."
    )
    moves = build_moves_list(moves_map)
    game = XOGame(7, moves, 5)
    result = game.make_move(24)[0]
    assert result is True


def test_winning_move_of_x_corner_element():
    moves_map = (
        ".OO.OO."
        "X......"
        "X......"
        "X......"
        "X......"
        "......."
        "......."
    )
    moves = build_moves_list(moves_map)
    game = XOGame(7, moves, 5)
    result = game.make_move(0)[0]
    assert result is True


def test_winning_move_of_x_corner_element_2():
    moves_map = (
        ".OO.OO."
        "......."
        "......."
        "......."
        "......."
        "......."
        "..XXXX."
    )
    moves = build_moves_list(moves_map)
    game = XOGame(7, moves, 5)
    result = game.make_move(48)[0]
    assert result is True


def test_winning_move_of_o_corner_element():
    moves_map = (
        ".OOOO.."
        "......."
        "......."
        "......."
        "......."
        "....X.."
        "..XXXX."
    )
    moves = build_moves_list(moves_map)
    game = XOGame(7, moves, 5)
    result = game.make_move(0)[0]
    assert result is True


def test_not_winning_move_of_x_not_enough_elements():
    moves_map = (
        ".O..OO."
        "......."
        "......."
        "......."
        "......."
        "......."
        "...XXX."
    )
    moves = build_moves_list(moves_map)
    game = XOGame(7, moves, 5)
    result = game.make_move(48)[0]
    assert result is False


def test_small_field_no_win():
    moves_map = (
        "X.X"
        ".X."
        "O.O"
    )
    moves = build_moves_list(moves_map)
    game = XOGame(3, moves)
    result = game.make_move(1)[0]
    assert result is False


def test_small_field_win():
    moves_map = (
        "X.X"
        ".X."
        "OO."
    )
    moves = build_moves_list(moves_map)
    game = XOGame(3, moves)
    result = game.make_move(8)[0]
    assert result is True
