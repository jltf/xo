from sqlalchemy import Column, Integer, Boolean, Enum, ARRAY

from sqlalchemy.orm import declarative_base

Base = declarative_base()

result_enum = Enum("win", "lose", "tie", name="result_enum")


class Game(Base):
    __tablename__ = 'games'

    id = Column(Integer, primary_key=True)
    player_moves_first = Column(Boolean, nullable=False)
    field_size = Column(Integer, nullable=False)
    moves = Column(ARRAY(Integer))
    result = Column(result_enum)

    def __repr__(self):
        return f"<Game(id={self.id})>"
