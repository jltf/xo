from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError

engine = create_engine('postgresql://postgres:example@db/xo')

try:
    conn = engine.connect()
except OperationalError:
    engine.dispose()
    engine = create_engine('postgresql://postgres:example@db')
    conn = engine.connect()
    conn.execute("commit")
    conn.execute("create database xo")
else:
    conn.close()
