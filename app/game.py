import asyncio
from itertools import cycle
from random import choice

from more_itertools import chunked


class XOGameException(Exception):
    """
    Base exception of XOGame class
    """
    pass


class MoveExists(XOGameException):
    """
    Move already exists on the board.
    """
    pass


class OutOfBoard(XOGameException):
    """
    Desired position not exists on the board.
    """
    pass


def generate_move(field_size, moves):
    """
    Generates new moves.
    """
    # Stands outside of class to be able to be called in process pool
    # executor or some other kind of executor.
    moves = list(set(range(0, field_size ** 2)) - moves)
    return choice(moves)


class XOGame:
    """
    XO game logic and algorithims.
    """
    def __init__(self, field_size, moves, num_to_win=5, executor=None):
        self._field_size = field_size
        self._num_to_win = min(num_to_win, field_size)

        self._next_symbol_cycle = cycle("XO")
        self._moves_map = self._build_moves_map(moves)
        self._moves_sequence = moves
        self._moves = set(moves)
        self._new_moves = list()
        self._executor = executor

    def _build_moves_map(self, moves):
        """
        Builds two-dimmensional representation of game.
        """
        moves_map = ["."] * self._field_size ** 2
        for move, symbol in zip(moves, self._next_symbol_cycle):
            moves_map[move] = symbol
        moves_map = list(chunked(moves_map, self._field_size))
        return moves_map

    def __str__(self):
        return "\n".join("".join(line) for line in self._moves_map)

    def __repr__(self):
        return f"<Game(field_size={self._field_size}, moves={self._moves})>"

    def check_move(self, row, col):
        """
        Checks if move at (row, col) position is winning.
        """
        directions = ((-1, 1), (0, 1), (1, 1), (1, 0))
        value = self._moves_map[row][col]

        for row_dir, col_dir in directions:
            # moving to the end of combo
            for offset in range(1, self._num_to_win + 1):
                row_index = row + offset * row_dir
                col_index = col + offset * col_dir
                try:
                    value_to_check = self._moves_map[row_index][col_index]
                except IndexError:
                    break
                if value_to_check != value:
                    break

            # move one element back
            row = row_index - row_dir
            col = col_index - col_dir

            # moving backwards to another end of combo and checking if
            # there is self._num_to_win elements in the sequence
            # required for a win.
            for offset in range(0, self._num_to_win):
                row_index = row + offset * -row_dir
                col_index = col + offset * -col_dir
                try:
                    value_to_check = self._moves_map[row_index][col_index]
                except IndexError:
                    break
                if value_to_check != value:
                    break
            else:
                return True

        return False

    def make_move(self, position):
        """
        Sets new move and checks which resoult it brought.
        """
        if position in self._moves:
            raise MoveExists

        if not (0 <= position < self._field_size ** 2):
            raise OutOfBoard

        self._new_moves.append(position)
        self._moves.add(position)
        row, col = divmod(position, self._field_size)
        symbol = next(self._next_symbol_cycle)
        self._moves_map[row][col] = symbol

        return self.check_move(row, col), self.all_moves_are_made(), symbol

    def fetch_new_moves(self):
        """
        Returns moves made after initialization.
        """
        new_moves = self._new_moves
        self._moves_sequence.extend(self._new_moves)
        self._new_moves = []
        return new_moves

    def get_all_moves(self):
        """
        Returns all moves made during game.
        """
        return self._moves_sequence + self._new_moves

    def all_moves_are_made(self):
        """
        Checks if there is no more moves allowed.
        """
        if len(self._moves) < self._field_size ** 2:
            return False
        else:
            return True

    async def generate_move(self):
        """
        Generates move based on current state of game.
        """
        # TODO algorithmic instead of randomizing.
        loop = asyncio.get_event_loop()
        result = await loop.run_in_executor(
            self._executor, generate_move, self._field_size, self._moves
        )
        return result

    async def make_generated_move(self):
        """
        Generates move and makes it.
        """
        generated_position = await self.generate_move()
        return generated_position, *self.make_move(generated_position)
