from game import XOGame


class SessionException(Exception):
    pass


class SessionNotFound(SessionException):
    pass


class GameSession:
    def __init__(self, game_id, redis_pool):
        self._key = self.name(game_id)
        self._redis_pool = redis_pool

    @staticmethod
    def name(game_id):
        return f"game_{game_id}_session"

    @staticmethod
    async def create_new(redis_pool, game, move=None):
        """
        Creates session representation of game in session storage.
        """
        key = GameSession.name(game.id)
        field_size = game.field_size
        await redis_pool.execute("rpush", key, field_size)
        if move:
            await redis_pool.execute("rpush", key, move)

    async def restore(self):
        """
        Returns XOGame instanse initiated by data stored in the
        game session.
        """
        try:
            field_size, *moves = await self._redis_pool.execute(
                "lrange", self._key, 0, -1
            )
        except ValueError:
            raise SessionNotFound

        return XOGame(int(field_size), [int(move) for move in moves])

    async def dump(self, game):
        """
        Saves new moves to session storage.
        """
        new_moves = game.fetch_new_moves()
        await self._redis_pool.execute("rpush", self._key, *new_moves)

    async def dispose(self):
        await self._redis_pool.execute("del", self._key)
