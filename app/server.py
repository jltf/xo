from concurrent.futures import ProcessPoolExecutor
from enum import Enum
from functools import partial
from json import JSONDecodeError
from random import randint

import aioredis
from aiohttp import web
from aioredlock import Aioredlock, LockError
from marshmallow import ValidationError
from sqlalchemy.exc import NoResultFound
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.future import select
from sqlalchemy.orm import sessionmaker

from game import MoveExists, OutOfBoard
from game_session import GameSession, SessionNotFound
from models import Game
from schemas import (
    NewGameSchema,
    NewGameResponseSchema,
    PlayersMoveSchema,
    PlayersMoveResponseSchema,
)

DEFAULT_FIELD_SIZE = 7
PLAYERS_MOVE_LOCK_TIMEOUT = 10
REDIS_URI = "redis://redis"
DB_URI = "postgresql+asyncpg://postgres:example@db/xo"


class Result(Enum):
    WIN = "win"
    LOSE = "lose"
    TIE = "tie"


async def create_game(request):
    """
    Creates new game and generates server side move if server had been
    chosen for first move.
    """
    try:
        data = await request.json()
    except JSONDecodeError:
        data = dict()

    try:
        new_game = NewGameSchema().load(data)
    except ValidationError as err:
        return web.json_response(err.messages)

    field_size = new_game.get("field_size", DEFAULT_FIELD_SIZE)
    db_engine = request.app["db_engine"]

    async_session = sessionmaker(
        db_engine, expire_on_commit=False, class_=AsyncSession
    )

    player_moves_first = randint(0, 1)

    # Create record for the new game in the database
    new_game = Game(
        player_moves_first=player_moves_first, field_size=field_size
    )
    async with async_session() as session:
        async with session.begin():
            session.add(new_game)
            await session.flush()

    # Generate server move if it moves first
    if not new_game.player_moves_first:
        generated_move = randint(0, field_size ** 2 - 1)
    else:
        generated_move = None

    # Create game session in redis for the new game
    await GameSession.create_new(
        request.app["redis_pool"],
        new_game,
        generated_move,
    )

    response = {
        "game_id": new_game.id,
        "field_size": field_size,
        "player_moves_first": new_game.player_moves_first,
        "servers_move": generated_move,
    }

    response = NewGameResponseSchema().dumps(response)
    return web.json_response(text=response)


async def get_db_game(game_id, db_engine):
    """
    Returns db game data.
    """
    async_session = sessionmaker(
        db_engine, expire_on_commit=False, class_=AsyncSession
    )
    stmt = select(Game).where(Game.id == game_id)

    async with async_session() as session:
        async with session.begin():
            result = await session.execute(stmt)

    game = result.one()[0]
    # TODO cache if result of game
    return game


class MoveFinalizer:
    """
    Unlocks session and return prepared response.
    """

    def __init__(self, unlock_func):
        self._unlock_func = unlock_func

    async def __call__(
            self, payload={}, game_over=False, message=None, status=200
    ):
        if message:
            payload.update({"message": message})
        payload.update({"game_over": game_over})
        await self._unlock_func()
        response = PlayersMoveResponseSchema().dumps(payload)
        return web.json_response(text=response, status=status)


def check_end_of_game(win, all_moves_are_made, symbol):
    """
    Translates game result into the result message if there is a
    someones win, or all moves are made.
    """
    message = None
    if win:
        message = f"{symbol} wins!"
    if all_moves_are_made:
        message = "All moves are made, game over."
    return message


class GameEndSaver:
    """
    Saves game stat to database and deletes session.
    """

    def __init__(self, db_engine, game_session):
        self._db_engine = db_engine
        self._game_session = game_session

    async def __call__(self, db_game, game, win, all_moves_are_made, symbol):
        players_symbol = "X" if db_game.player_moves_first else "O"
        if win:
            player_won = symbol == players_symbol
            result = Result.WIN.value if player_won else Result.LOSE.value
        elif all_moves_are_made:
            result = Result.TIE.value

        db_game.result = result
        db_game.moves = game.get_all_moves()

        async_session = sessionmaker(self._db_engine, class_=AsyncSession)
        async with async_session() as session:
            async with session.begin():
                session.add(db_game)
                await session.flush()

        await self._game_session.dispose()


async def make_move(request):
    """
    Processes players move and sends server side move or game result.
    """
    game_id = int(request.match_info.get("id"))

    # TODO player + game lock key when registration will be implemented
    # Distributed lock is used to prevent player for making simultanious
    # multiple turns.
    redlock = request.app["redlock"]
    lock_name = f"game_{game_id}_player_turn_lock"
    try:
        if await redlock.is_locked(lock_name):
            raise LockError
        player_move_lock = await redlock.lock(
            lock_name, lock_timeout=PLAYERS_MOVE_LOCK_TIMEOUT
        )
    except LockError:
        return web.json_response(
            {"message": "Can't make a move, try again later."}
        )

    db_engine = request.app["db_engine"]
    finalize_move = MoveFinalizer(partial(redlock.unlock, player_move_lock))

    # Check if game is already played.
    try:
        db_game = await get_db_game(game_id, db_engine)
    except NoResultFound:
        return await finalize_move(message="Game not exists.")
    if db_game.result:
        return await finalize_move(message="Game is already played.")

    # TODO check if game belongs to player when there will be registration.

    data = await request.json()
    try:
        PlayersMoveSchema().load(data)
    except ValidationError as err:
        return web.json_response(err.messages)

    players_move = int(data["position"])
    redis_pool = request.app["redis_pool"]

    # Restore game object from game session.
    game_session = GameSession(game_id, redis_pool)
    try:
        game = await game_session.restore()
    except SessionNotFound:
        return await finalize_move(message="Game session is not found.")

    # Make players move.
    try:
        move_result = game.make_move(players_move)
        print("Player:", game, flush=True)
    except MoveExists:
        return await finalize_move(
            message="Move is not allowed: desired position is taken."
        )
    except OutOfBoard:
        return await finalize_move(
            message="Move is not allowed: no such position on the board."
        )

    # Check players move result.
    save = GameEndSaver(db_engine, game_session)
    if game_end_message := check_end_of_game(*move_result):
        await save(db_game, game, *move_result)
        return await finalize_move(message=game_end_message, game_over=True)

    # Generation of move is potentially long operation so it's made as
    # coroutine, and it will be executed in process executor pool.
    generated_position, *move_result = await game.make_generated_move()
    print("Server:", game, flush=True)
    if game_end_message := check_end_of_game(*move_result):
        await save(db_game, game, *move_result)
        return await finalize_move(
            game_over=True,
            payload={
                "message": game_end_message,
                "servers_move": generated_position
            }
        )

    await game_session.dump(game)

    return await finalize_move(payload={"servers_move": generated_position})


async def sqlalchemy_engine(app):
    engine = create_async_engine(DB_URI, echo=True)
    app["db_engine"] = engine
    yield
    await engine.dispose()


async def redis_pool(app):
    redis_pool = await aioredis.create_pool(REDIS_URI)
    app["redis_pool"] = redis_pool
    yield
    redis_pool.close()
    await redis_pool.wait_closed()


async def redlock_manager(app):
    redis_instances = [REDIS_URI]
    lock_manager = Aioredlock(redis_instances)
    app["redlock"] = lock_manager
    yield
    await lock_manager.destroy()


app = web.Application()
app["executor"] = ProcessPoolExecutor(max_workers=2)
app.cleanup_ctx.append(redis_pool)
app.cleanup_ctx.append(sqlalchemy_engine)
app.cleanup_ctx.append(redlock_manager)

app.add_routes(
    [
        web.post("/game", create_game),
        web.patch("/game/{id}", make_move),
    ]
)

if __name__ == "__main__":
    web.run_app(app)
