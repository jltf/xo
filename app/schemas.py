from marshmallow import Schema, fields, EXCLUDE
from marshmallow.validate import Range


class NewGameSchema(Schema):
    field_size = fields.Int(validate=[Range(min=3)])

    class Meta:
        unknown = EXCLUDE


class NewGameResponseSchema(Schema):
    game_id = fields.Int(validate=[Range(min=0)], required=True)
    field_size = fields.Int(validate=[Range(min=3)], requried=True)
    player_moves_first = fields.Boolean(required=True)
    servers_move = fields.Int(
        validate=[Range(min=0)], allow_none=True, required=True
    )


class PlayersMoveSchema(Schema):
    position = fields.Int(required=True, validate=Range(min=0))

    class Meta:
        unknown = EXCLUDE


class PlayersMoveResponseSchema(Schema):
    message = fields.String(required=False)
    servers_move = fields.String(validate=[Range(min=0)], required=False)
    game_over = fields.Boolean()
