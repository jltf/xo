from functools import partial, wraps


def build_cache_key(func, args):
    return 'cache-{}-{}'.format(
        func.__name__, '-'.join(str(arg) for arg in args)
    )


def cache(func=None, *, cache_name=None, timeout=60):
    if func is None:
        return partial(cache, timeout=timeout)

    @wraps(func)
    async def wrapper(*args, **kwargs):
        request, *key_args = args
        key = build_cache_key(func, args)
        cached_result = await request.app['redis_pool'].execute('get', key)
        if cached_result:
            return cached_result.decode('utf-8')

        result = await func(*args, **kwargs)

        await request.app['redis_pool'].execute('set', key, result)
        await request.app['redis_pool'].execute('expire', key, timeout)

        return result
    return wrapper
