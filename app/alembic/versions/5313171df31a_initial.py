"""Initial

Revision ID: 5313171df31a
Revises:
Create Date: 2021-04-03 15:39:52.394631

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = "5313171df31a"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "games",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("player_moves_first", sa.Boolean(), nullable=False),
        sa.Column("field_size", sa.Integer(), nullable=False),
        sa.Column("moves", sa.ARRAY(sa.Integer()), nullable=True),
        sa.Column(
            "result",
            sa.Enum("win", "lose", "tie", name="result_enum"),
            nullable=True,
        ),
        sa.PrimaryKeyConstraint("id"),
    )


def downgrade():
    op.drop_table("games")
    op.execute('DROP TYPE ' + "result_enum")
