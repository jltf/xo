FROM python:latest

COPY requirements.txt /requirements.txt
RUN pip install --no-cache-dir -r /requirements.txt

WORKDIR /app

CMD ["python", "server.py"]
